import React from 'react';
import ReactDOM from 'react-dom';
import Header from "./component/header";
import Main from './component/main';
const App= () =>{
return (
    <div>
    <Header />
    <Main />
    </div>
)
}

ReactDOM.render(<App />, document.getElementById('root'));

